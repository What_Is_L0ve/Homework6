﻿using System;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders;

public class ThreadPoolProcessItem
{
    public ThreadPoolProcessItem(int threadKey, Action<int> onFinishCallback)
    {
        ThreadKey = threadKey;
        WaitHandle = new AutoResetEvent(false);
        OnFinished = onFinishCallback;
    }

    public int ThreadKey { get; }

    public WaitHandle WaitHandle { get; }

    public Action<int> OnFinished { get; set; }

    public void OnFinish()
    {
        OnFinished(ThreadKey);
    }
}